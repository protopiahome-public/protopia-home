## IT-продукты: Protopia Ecosystem и другие

Дом Протопии разрабатывает множество IT-продуктов, полезных для сообществ и сетевых организаций. Все эти продукты разрабатываются как Open Source. 
 
В первую очередь это протокол и семейство фреймворков Protopia Ecosystem: [https://gitlab.com/protopiahome-public/protopia-ecosystem](https://gitlab.com/protopiahome-public/protopia-ecosystem) На них основаны остальные наши продукты. Суть этой технологии в том, чтобы любой мог создать произвольное число функциональных узлов (например, веб-сервисов, react-приложений, телеграм-ботов), которые могут соединяться друг с другом, обмениваться данными, а также авторизировать пользователей друг через друга. 
 
Наша собственная система управления: [https://gitlab.com/protopiahome-public/it-public/protopia-home-manage](https://gitlab.com/protopiahome-public/it-public/protopia-home-manage) Она позволяет управлять сообществом и организацией на основе методологий вроде Социократии 3.0. Она несет множество функций, полезных сообществу - например, авторепостинг постов из ВК в телеграм- и ВК-чаты. Вы можете увидеть ее в действии в нашем Телеграм-боте: [https://t.me/protopia_beta_bot](https://t.me/protopia_beta_bot) и если присоединитесь в один из наших организационных кругов. Мы также в процессе универсализации этой системы управления в виде системы S3APP: [https://gitlab.com/protopiahome-public/it-public/s3app](https://gitlab.com/protopiahome-public/it-public/s3app) 
 
Бот социократии: [https://gitlab.com/protopiahome-public/it-public/sociocracy30_public](https://gitlab.com/protopiahome-public/it-public/sociocracy30_public) Бот, позволяющий проводить голосования по консенту. Это часть функционала, включенного в бот Дома Протопии. Воспользоваться ботом вы можете по адресу [https://t.me/sociocracy30_bot](https://t.me/sociocracy30_bot) 
 
Community Map: [https://gitlab.com/protopiahome-public/it-public/community-map](https://gitlab.com/protopiahome-public/it-public/community-map) Это географическая карта, позволяющая добавлять на нее места, связанные с ней сообщества и события. Пример ее практического применения для форума "Ладога": [https://ladoga.communityhub.ru/](https://ladoga.communityhub.ru/) 
 
Ресурсоворот: [https://gitlab.com/protopiahome-public/it-public/crowdresourcing](https://gitlab.com/protopiahome-public/it-public/crowdresourcing) Это как краудфандинг, но без денег. Вместо денег люди предлагают свои ресурсы и свою деятельность. Можно посмотреть на сайте [http://crowdresourcing.ru/](http://crowdresourcing.ru/) 
 
Flowmaster: [https://gitlab.com/protopiahome-public/it-public/flowmaster](https://gitlab.com/protopiahome-public/it-public/flowmaster) Это система мониторинга сообщений в разных чатах (таких как Телеграм, ВК, Slack) и репостинг их в другие чаты на основе лингвистического анализа. 
 
Oraculi: [https://gitlab.com/protopiahome-public/it-public/oraculi](https://gitlab.com/protopiahome-public/it-public/oraculi) - это система электронного образование на базе Protopia Ecosystem. Функционально близка к moodle, но микросервисная и с более современным технологическим стеком. 
 
Другие наши (а также близкие к нам, размещенные в наших репозиториях) IT-разработки вы можете посмотреть здесь: [https://gitlab.com/protopiahome-public/it-public](https://gitlab.com/protopiahome-public/it-public) 
 
Мы будем разрабатывать их быстрее, если вы будете нам помогать. Вы можете присоединяйтесь к чату их разработки [https://t.me/protopia_framework](https://t.me/protopia_framework)
